from django.conf.urls import url
from django.urls import path, include

from .views import *

urlpatterns = [
    path("", include('django.contrib.auth.urls')),
    path("", home, name="home"),

    path("register", register, name="register"),
    path("register-owner", add_owner, name="add-owner"),
    path("register-user", add_client, name="add-client"),
    path("register-restaurant", add_restaurant, name="add-restaurant"),
    path("client/", client_profile, name="client"),
    url("client/(?P<name>.+)", client_profile, name="client"),
    path("owner/", owner_profile, name="owner"),
    url("owner/(?P<name>.+)", owner_profile, name="owner"),
    path("my-restaurants", owner_restaurants, name="my-restaurants"),
    path("edit-restaurant/<int:number>", edit_restaurant, name="edit-restaurant"),
    path("restaurant/<int:number>", restaurant_detail, name="restaurant-detail"),
    path("list-restaurants", restaurant_listing, name="restaurant-listing"),
    path("my-favorites", favorites_listing, name="favourites_listing"),
    path("reload-database", reload_database, name="reload_database"),
]
