from django import forms


class AddPerson(forms.Form):
    email = forms.EmailField(label="Email", help_text="Insert Email")
    first_name = forms.CharField(label="First Name", help_text="Insert First Name")
    last_name = forms.CharField(label="Last Name", help_text="Insert Last Name")
    phone_number = forms.CharField(label="Phone Number", help_text="Insert Phone Number")
    photo = forms.ImageField(label="Photo", required=False)
    password = forms.CharField(label="Password", required=False, help_text="Password",
                               widget=forms.PasswordInput())
    repeat_password = forms.CharField(label="Repeat Password", required=False, help_text="Repeat Password",
                                      widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name != "photo":
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "input100"


class AddRestaurant(forms.Form):
    name = forms.CharField(label="Name", help_text="Insert Name")
    address = forms.CharField(label="Address", help_text="Insert Address")
    city = forms.CharField(label="City", help_text="Insert City")
    zip_code = forms.CharField(label="Zip Code", help_text="Insert Zip Code")
    phone_number = forms.CharField(label="Phone Number", help_text="Insert Phone Number")

    country_choices = (
        ("Afghanistan", "Afghanistan"), ("Albania", "Albania"), ("Algeria", "Algeria"), ("Andorra", "Andorra"),
        ("Angola", "Angola"), ("Antigua & Deps", "Antigua & Deps"), ("Argentina", "Argentina"),
        ("Armenia", "Armenia"), ("Australia", "Australia"), ("Austria", "Austria"), ("Azerbaijan", "Azerbaijan"),
        ("Bahamas", "Bahamas"), ("Bahrain", "Bahrain"), ("Bangladesh", "Bangladesh"), ("Barbados", "Barbados"),
        ("Belarus", "Belarus"), ("Belgium", "Belgium"), ("Belize", "Belize"), ("Benin", "Benin"), ("Bhutan", "Bhutan"),
        ("Bolivia", "Bolivia"), ("Bosnia Herzegovina", "Bosnia Herzegovina"), ("Botswana", "Botswana"),
        ("Brazil", "Brazil"), ("Brunei", "Brunei"), ("Bulgaria", "Bulgaria"), ("Burkina", "Burkina"),
        ("Burundi", "Burundi"), ("Cambodia", "Cambodia"), ("Cameroon", "Cameroon"), ("Canada", "Canada"),
        ("Cape Verde", "Cape Verde"), ("Central African Rep", "Central African Rep"), ("Chad", "Chad"),
        ("Chile", "Chile"), ("China", "China"), ("Colombia", "Colombia"), ("Comoros", "Comoros"), ("Congo", "Congo"),
        ("Congo Democratic Rep", "Congo Democratic Rep"), ("Costa Rica", "Costa Rica"), ("Croatia", "Croatia"),
        ("Cuba", "Cuba"), ("Cyprus", "Cyprus"), ("Czech Republic", "Czech Republic"), ("Denmark", "Denmark"),
        ("Djibouti", "Djibouti"), ("Dominica", "Dominica"), ("Dominican Republic", "Dominican Republic"),
        ("East Timor", "East Timor"), ("Ecuador", "Ecuador"), ("Egypt", "Egypt"), ("El Salvador", "El Salvador"),
        ("Equatorial Guinea", "Equatorial Guinea"), ("Eritrea", "Eritrea"), ("Estonia", "Estonia"),
        ("Ethiopia", "Ethiopia"), ("Fiji", "Fiji"), ("Finland", "Finland"), ("France", "France"), ("Gabon", "Gabon"),
        ("Gambia", "Gambia"), ("Georgia", "Georgia"), ("Germany", "Germany"), ("Ghana", "Ghana"), ("Greece", "Greece"),
        ("Grenada", "Grenada"), ("Guatemala", "Guatemala"), ("Guinea", "Guinea"), ("Guinea-Bissau", "Guinea-Bissau"),
        ("Guyana", "Guyana"), ("Haiti", "Haiti"), ("Honduras", "Honduras"), ("Hungary", "Hungary"),
        ("Iceland", "Iceland"), ("India", "India"), ("Indonesia", "Indonesia"), ("Iran", "Iran"), ("Iraq", "Iraq"),
        ("Ireland Republic", "Ireland Republic"), ("Israel", "Israel"), ("Italy", "Italy"),
        ("Ivory Coast", "Ivory Coast"), ("Jamaica", "Jamaica"), ("Japan", "Japan"), ("Jordan", "Jordan"),
        ("Kazakhstan", "Kazakhstan"), ("Kenya", "Kenya"), ("Kiribati", "Kiribati"), ("Korea North", "Korea North"),
        ("Korea South", "Korea South"), ("Kosovo", "Kosovo"), ("Kuwait", "Kuwait"), ("Kyrgyzstan", "Kyrgyzstan"),
        ("Laos", "Laos"), ("Latvia", "Latvia"), ("Lebanon", "Lebanon"), ("Lesotho", "Lesotho"), ("Liberia", "Liberia"),
        ("Libya", "Libya"), ("Liechtenstein", "Liechtenstein"), ("Lithuania", "Lithuania"),
        ("Luxembourg", "Luxembourg"), ("Macedonia", "Macedonia"), ("Madagascar", "Madagascar"), ("Malawi", "Malawi"),
        ("Malaysia", "Malaysia"), ("Maldives", "Maldives"), ("Mali", "Mali"), ("Malta", "Malta"),
        ("Marshall Islands", "Marshall Islands"), ("Mauritania", "Mauritania"), ("Mauritius", "Mauritius"),
        ("Mexico", "Mexico"), ("Micronesia", "Micronesia"), ("Moldova", "Moldova"), ("Monaco", "Monaco"),
        ("Mongolia", "Mongolia"), ("Montenegro", "Montenegro"), ("Morocco", "Morocco"), ("Mozambique", "Mozambique"),
        ("Myanmar", "Myanmar"), ("Namibia", "Namibia"), ("Nauru", "Nauru"), ("Nepal", "Nepal"),
        ("Netherlands", "Netherlands"), ("New Zealand", "New Zealand"), ("Nicaragua", "Nicaragua"), ("Niger", "Niger"),
        ("Nigeria", "Nigeria"), ("Norway", "Norway"), ("Oman", "Oman"), ("Pakistan", "Pakistan"), ("Palau", "Palau"),
        ("Panama", "Panama"), ("Papua New Guinea", "Papua New Guinea"), ("Paraguay", "Paraguay"), ("Peru", "Peru"),
        ("Philippines", "Philippines"), ("Poland", "Poland"), ("Portugal", "Portugal"), ("Qatar", "Qatar"),
        ("Romania", "Romania"), ("Russian Federation", "Russian Federation"), ("Rwanda", "Rwanda"),
        ("St Kitts & Nevis", "St Kitts & Nevis"), ("St Lucia", "St Lucia"),
        ("Saint Vincent & the Grenadines", "Saint Vincent & the Grenadines"), ("Samoa", "Samoa"),
        ("San Marino", "San Marino"), ("Sao Tome & Principe", "Sao Tome & Principe"), ("Saudi Arabia", "Saudi Arabia"),
        ("Senegal", "Senegal"), ("Serbia", "Serbia"), ("Seychelles", "Seychelles"), ("Sierra Leone", "Sierra Leone"),
        ("Singapore", "Singapore"), ("Slovakia", "Slovakia"), ("Slovenia", "Slovenia"),
        ("Solomon Islands", "Solomon Islands"), ("Somalia", "Somalia"), ("South Africa", "South Africa"),
        ("South Sudan", "South Sudan"), ("Spain", "Spain"), ("Sri Lanka", "Sri Lanka"), ("Sudan", "Sudan"),
        ("Suriname", "Suriname"), ("Swaziland", "Swaziland"), ("Sweden", "Sweden"), ("Switzerland", "Switzerland"),
        ("Syria", "Syria"), ("Taiwan", "Taiwan"), ("Tajikistan", "Tajikistan"), ("Tanzania", "Tanzania"),
        ("Thailand", "Thailand"), ("Togo", "Togo"), ("Tonga", "Tonga"), ("Trinidad & Tobago", "Trinidad & Tobago"),
        ("Tunisia", "Tunisia"), ("Turkey", "Turkey"), ("Turkmenistan", "Turkmenistan"), ("Tuvalu", "Tuvalu"),
        ("Uganda", "Uganda"), ("Ukraine", "Ukraine"), ("United Arab Emirates", "United Arab Emirates"),
        ("United Kingdom", "United Kingdom"), ("United States", "United States"), ("Uruguay", "Uruguay"),
        ("Uzbekistan", "Uzbekistan"), ("Vanuatu", "Vanuatu"), ("Vatican City", "Vatican City"),
        ("Venezuela", "Venezuela"), ("Vietnam", "Vietnam"), ("Yemen", "Yemen"), ("Zambia", "Zambia"),
        ("Zimbabwe", "Zimbabwe"))
    country = forms.ChoiceField(label="Country", choices=country_choices)

    latitude = forms.FloatField(label="Latitude", help_text="Insert Latitude")
    longitude = forms.CharField(label="Longitude", help_text="Insert Longitude")
    photo = forms.ImageField(label="Photo", required=False)

    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name != "photo" and field_name != "country":
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "input100"

            elif field_name == "country":
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "form-control"


class UpdateClient(forms.Form):
    email = forms.EmailField(label="Email", help_text="Insert Email")
    first_name = forms.CharField(label="First Name", help_text="Insert First Name")
    last_name = forms.CharField(label="Last Name", help_text="Insert Last Name")
    phone_number = forms.CharField(label="Phone Number", help_text="Insert Phone Number")
    photo = forms.ImageField(label="Photo", required=False)
    new_password = forms.CharField(label="New Password", required=False, help_text="New Password",
                                   widget=forms.PasswordInput())
    repeat_password = forms.CharField(label="Repeat New Password", required=False, help_text="Repeat New Password",
                                      widget=forms.PasswordInput())

    def __init__(self, user, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs["placeholder"] = field.help_text
            field.widget.attrs["class"] = "input100"

        if user is not None:
            for field_name, field in self.fields.items():
                if field_name == "first_name":
                    field.initial = user.user.first_name

                if field_name == "last_name":
                    field.initial = user.user.last_name

                if field_name == "email":
                    field.initial = user.user.email

                if field_name == "phone_number":
                    field.initial = user.phone_number

                if field_name == "photo":
                    field.widget.attrs["class"] = "col-md-3 col-sm-3"
                    field.initial = "data:image/png;base64," + user.photo


class UpdateOwner(forms.Form):
    email = forms.EmailField(label="Email", help_text="Insert Email")
    first_name = forms.CharField(label="First Name", help_text="Insert First Name")
    last_name = forms.CharField(label="Last Name", help_text="Insert Last Name")
    phone_number = forms.CharField(label="Phone Number", help_text="Insert Phone Number")
    photo = forms.ImageField(label="Photo", required=False)
    new_password = forms.CharField(label="New Password", required=False, help_text="New Password",
                                   widget=forms.PasswordInput())
    repeat_password = forms.CharField(label="Repeat New Password", required=False, help_text="Repeat New Password",
                                      widget=forms.PasswordInput())

    def __init__(self, user, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs["placeholder"] = field.help_text
            field.widget.attrs["class"] = "input100"

        if user is not None:
            for field_name, field in self.fields.items():
                if field_name == "first_name":
                    field.initial = user.user.first_name

                if field_name == "last_name":
                    field.initial = user.user.last_name

                if field_name == "email":
                    field.initial = user.user.email

                if field_name == "phone_number":
                    field.initial = user.phone_number

                if field_name == "photo":
                    field.widget.attrs["class"] = "col-md-3 col-sm-3"
                    field.initial = "data:image/png;base64," + user.photo


class EditRestaurant(forms.Form):
    name = forms.CharField(label="Name", help_text="Insert Name", disabled=True, required=False)
    address = forms.CharField(label="Address", help_text="Insert Address")
    city = forms.CharField(label="City", help_text="Insert City", disabled=True, required=False)
    zip_code = forms.CharField(label="Zip Code", help_text="Insert Zip Code")
    phone_number = forms.CharField(label="Phone Number", help_text="Insert Phone Number")

    country_choices = (
        ("Afghanistan", "Afghanistan"), ("Albania", "Albania"), ("Algeria", "Algeria"), ("Andorra", "Andorra"),
        ("Angola", "Angola"), ("Antigua & Deps", "Antigua & Deps"), ("Argentina", "Argentina"),
        ("Armenia", "Armenia"), ("Australia", "Australia"), ("Austria", "Austria"), ("Azerbaijan", "Azerbaijan"),
        ("Bahamas", "Bahamas"), ("Bahrain", "Bahrain"), ("Bangladesh", "Bangladesh"), ("Barbados", "Barbados"),
        ("Belarus", "Belarus"), ("Belgium", "Belgium"), ("Belize", "Belize"), ("Benin", "Benin"), ("Bhutan", "Bhutan"),
        ("Bolivia", "Bolivia"), ("Bosnia Herzegovina", "Bosnia Herzegovina"), ("Botswana", "Botswana"),
        ("Brazil", "Brazil"), ("Brunei", "Brunei"), ("Bulgaria", "Bulgaria"), ("Burkina", "Burkina"),
        ("Burundi", "Burundi"), ("Cambodia", "Cambodia"), ("Cameroon", "Cameroon"), ("Canada", "Canada"),
        ("Cape Verde", "Cape Verde"), ("Central African Rep", "Central African Rep"), ("Chad", "Chad"),
        ("Chile", "Chile"), ("China", "China"), ("Colombia", "Colombia"), ("Comoros", "Comoros"), ("Congo", "Congo"),
        ("Congo Democratic Rep", "Congo Democratic Rep"), ("Costa Rica", "Costa Rica"), ("Croatia", "Croatia"),
        ("Cuba", "Cuba"), ("Cyprus", "Cyprus"), ("Czech Republic", "Czech Republic"), ("Denmark", "Denmark"),
        ("Djibouti", "Djibouti"), ("Dominica", "Dominica"), ("Dominican Republic", "Dominican Republic"),
        ("East Timor", "East Timor"), ("Ecuador", "Ecuador"), ("Egypt", "Egypt"), ("El Salvador", "El Salvador"),
        ("Equatorial Guinea", "Equatorial Guinea"), ("Eritrea", "Eritrea"), ("Estonia", "Estonia"),
        ("Ethiopia", "Ethiopia"), ("Fiji", "Fiji"), ("Finland", "Finland"), ("France", "France"), ("Gabon", "Gabon"),
        ("Gambia", "Gambia"), ("Georgia", "Georgia"), ("Germany", "Germany"), ("Ghana", "Ghana"), ("Greece", "Greece"),
        ("Grenada", "Grenada"), ("Guatemala", "Guatemala"), ("Guinea", "Guinea"), ("Guinea-Bissau", "Guinea-Bissau"),
        ("Guyana", "Guyana"), ("Haiti", "Haiti"), ("Honduras", "Honduras"), ("Hungary", "Hungary"),
        ("Iceland", "Iceland"), ("India", "India"), ("Indonesia", "Indonesia"), ("Iran", "Iran"), ("Iraq", "Iraq"),
        ("Ireland Republic", "Ireland Republic"), ("Israel", "Israel"), ("Italy", "Italy"),
        ("Ivory Coast", "Ivory Coast"), ("Jamaica", "Jamaica"), ("Japan", "Japan"), ("Jordan", "Jordan"),
        ("Kazakhstan", "Kazakhstan"), ("Kenya", "Kenya"), ("Kiribati", "Kiribati"), ("Korea North", "Korea North"),
        ("Korea South", "Korea South"), ("Kosovo", "Kosovo"), ("Kuwait", "Kuwait"), ("Kyrgyzstan", "Kyrgyzstan"),
        ("Laos", "Laos"), ("Latvia", "Latvia"), ("Lebanon", "Lebanon"), ("Lesotho", "Lesotho"), ("Liberia", "Liberia"),
        ("Libya", "Libya"), ("Liechtenstein", "Liechtenstein"), ("Lithuania", "Lithuania"),
        ("Luxembourg", "Luxembourg"), ("Macedonia", "Macedonia"), ("Madagascar", "Madagascar"), ("Malawi", "Malawi"),
        ("Malaysia", "Malaysia"), ("Maldives", "Maldives"), ("Mali", "Mali"), ("Malta", "Malta"),
        ("Marshall Islands", "Marshall Islands"), ("Mauritania", "Mauritania"), ("Mauritius", "Mauritius"),
        ("Mexico", "Mexico"), ("Micronesia", "Micronesia"), ("Moldova", "Moldova"), ("Monaco", "Monaco"),
        ("Mongolia", "Mongolia"), ("Montenegro", "Montenegro"), ("Morocco", "Morocco"), ("Mozambique", "Mozambique"),
        ("Myanmar", "Myanmar"), ("Namibia", "Namibia"), ("Nauru", "Nauru"), ("Nepal", "Nepal"),
        ("Netherlands", "Netherlands"), ("New Zealand", "New Zealand"), ("Nicaragua", "Nicaragua"), ("Niger", "Niger"),
        ("Nigeria", "Nigeria"), ("Norway", "Norway"), ("Oman", "Oman"), ("Pakistan", "Pakistan"), ("Palau", "Palau"),
        ("Panama", "Panama"), ("Papua New Guinea", "Papua New Guinea"), ("Paraguay", "Paraguay"), ("Peru", "Peru"),
        ("Philippines", "Philippines"), ("Poland", "Poland"), ("Portugal", "Portugal"), ("Qatar", "Qatar"),
        ("Romania", "Romania"), ("Russian Federation", "Russian Federation"), ("Rwanda", "Rwanda"),
        ("St Kitts & Nevis", "St Kitts & Nevis"), ("St Lucia", "St Lucia"),
        ("Saint Vincent & the Grenadines", "Saint Vincent & the Grenadines"), ("Samoa", "Samoa"),
        ("San Marino", "San Marino"), ("Sao Tome & Principe", "Sao Tome & Principe"), ("Saudi Arabia", "Saudi Arabia"),
        ("Senegal", "Senegal"), ("Serbia", "Serbia"), ("Seychelles", "Seychelles"), ("Sierra Leone", "Sierra Leone"),
        ("Singapore", "Singapore"), ("Slovakia", "Slovakia"), ("Slovenia", "Slovenia"),
        ("Solomon Islands", "Solomon Islands"), ("Somalia", "Somalia"), ("South Africa", "South Africa"),
        ("South Sudan", "South Sudan"), ("Spain", "Spain"), ("Sri Lanka", "Sri Lanka"), ("Sudan", "Sudan"),
        ("Suriname", "Suriname"), ("Swaziland", "Swaziland"), ("Sweden", "Sweden"), ("Switzerland", "Switzerland"),
        ("Syria", "Syria"), ("Taiwan", "Taiwan"), ("Tajikistan", "Tajikistan"), ("Tanzania", "Tanzania"),
        ("Thailand", "Thailand"), ("Togo", "Togo"), ("Tonga", "Tonga"), ("Trinidad & Tobago", "Trinidad & Tobago"),
        ("Tunisia", "Tunisia"), ("Turkey", "Turkey"), ("Turkmenistan", "Turkmenistan"), ("Tuvalu", "Tuvalu"),
        ("Uganda", "Uganda"), ("Ukraine", "Ukraine"), ("United Arab Emirates", "United Arab Emirates"),
        ("United Kingdom", "United Kingdom"), ("United States", "United States"), ("Uruguay", "Uruguay"),
        ("Uzbekistan", "Uzbekistan"), ("Vanuatu", "Vanuatu"), ("Vatican City", "Vatican City"),
        ("Venezuela", "Venezuela"), ("Vietnam", "Vietnam"), ("Yemen", "Yemen"), ("Zambia", "Zambia"),
        ("Zimbabwe", "Zimbabwe"))
    country = forms.ChoiceField(label="Country", choices=country_choices, disabled=True, required=False)

    latitude = forms.FloatField(label="Latitude", help_text="Insert Latitude")
    longitude = forms.CharField(label="Longitude", help_text="Insert Longitude")
    photo = forms.ImageField(label="Photo", required=False)

    def __init__(self, rest, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name != "photo" and field_name != "country":
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "input100"

            elif field_name == "country":
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "form-control"

        if rest is not None:
            for field_name, field in self.fields.items():
                if field_name == "name":
                    field.initial = rest.name
                    field.widget.attrs["readonly"] = True

                if field_name == "address":
                    field.initial = rest.address

                if field_name == "city":
                    field.initial = rest.city
                    field.widget.attrs["readonly"] = True

                if field_name == "zip_code":
                    field.initial = rest.zip_code

                if field_name == "country":
                    field.initial = rest.country
                    field.widget.attrs["readonly"] = True

                if field_name == "latitude":
                    field.initial = rest.latitude

                if field_name == "longitude":
                    field.initial = rest.longitude

                if field_name == "phone_number":
                    field.initial = rest.phone_number

                if field_name == "photo":
                    field.initial = f"data:image/png;base64,{rest.photo}"


class AddReview(forms.Form):
    title = forms.CharField(label="Title", help_text="Insert Title")
    comment = forms.CharField(label="Comment", help_text="Insert Comment")
    score = forms.FloatField(label="Score", help_text="Insert Score", max_value=10.0, min_value=0.0)

    price_choices = ((1, "Cheap"), (2, "Affordable"), (3, "Pricey"))
    price = forms.ChoiceField(label="Price", choices=price_choices)

    quality_choices = ((1, "Mediocre"), (2, "Average"), (3, "Good"))
    quality = forms.ChoiceField(label="Quality", choices=quality_choices)

    cleanliness_choices = ((1, "Dirty"), (2, "Average"), (3, "Clean"))
    cleanliness = forms.ChoiceField(label="Cleanliness", choices=cleanliness_choices)

    speed_choices = ((1, "Quick"), (2, "Average"), (3, "Slow"))
    speed = forms.ChoiceField(label="Speed", choices=speed_choices)

    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name == "score" or field_name == "comment" or field_name == "title":
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "input100"

            else:
                field.widget.attrs["placeholder"] = field.help_text
                field.widget.attrs["class"] = "form-control"


class SearchRestaurant(forms.Form):
    sort_choices = (
        (0, "Best Match"),
        (1, "Price (Low to High)"),
        (2, "Price (High to Low)"),
        (3, "Name (Ascending)"),
        (4, "Name (Descending)"),
        (6, "Classification (Low to High)"),
        (7, "Classification (High to Low)"),
        (8, "Number of Reviews"),
        (5, "Cleanliness"),
        (9, "Speed")
    )
    sort_by = forms.ChoiceField(
        label="Sort by:",
        choices=sort_choices,
        widget=forms.Select(attrs={'class': "custom-select mb-2 mr-sm-2 mb-sm-0", 'onchange': 'submit();'}),
        required=False
    )
    name = forms.CharField(
        label="Name",
        help_text="Restaurant Name",
        widget=forms.TextInput(attrs={'class': "btn-group1", 'placeholder': "What are your looking for?"}),
        required=False
    )
    city = forms.CharField(
        label="City",
        help_text="City",
        widget=forms.TextInput(attrs={'class': "btn-group2", 'placeholder': "City"}),
        required=False
    )

    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            if field_name != "sort_by":
                field.widget.attrs["placeholder"] = field.help_text
