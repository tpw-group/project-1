import base64

from django.contrib import messages
from django.contrib.auth.models import Group
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from .forms import *
from .models import *

from .variables import *


def register(request):
    return render(request, "base_register.html")


@csrf_exempt
def home(request):
    if request.method == "POST":
        request.session['_post_data'] = request.POST
        return redirect('restaurant-listing')
    else:
        data = []
        restaurants = Restaurant.objects.all()
        for rest in restaurants:
            elem_data = {"id": rest.id, "name": rest.name, "address": rest.address, "city": rest.city,
                         "country": rest.country,
                         "latitude": rest.latitude, "longitude": rest.longitude, "photo": rest.photo,
                         "is_open": rest.is_open, "phone_number": rest.phone_number, "description": rest.description}

            if request.user.is_authenticated and Client.objects.filter(user=request.user).exists():
                client = Client.objects.get(user=request.user)
                get_restaurant_details(rest, elem_data, client)
                elem_data["user_fav"] = rest in client.favorites.all()
            else:
                get_restaurant_details(rest, elem_data)

            data.append(elem_data)

        data.sort(key=lambda e: e['average_score'], reverse=True)
        return render(request, "home.html", {'restaurants': data[:2]})


@csrf_exempt
def add_owner(request):
    if request.method == "POST":
        form = AddPerson(request.POST, request.FILES)
        error = False
        if form.is_valid():

            # get info from data
            form_data = form.cleaned_data
            email = form_data["email"]
            first_name = form_data["first_name"]
            last_name = form_data["last_name"]
            phone_number = form_data["phone_number"]
            password = form_data["password"]
            repeat_password = form_data["repeat_password"]

            try:
                validate_password(password)
                if password != repeat_password:
                    form.add_error("password", "Password must be the same!")
                    form.add_error("repeat_password", "Password must be the same!")
                    error = True
            except ValidationError as e:
                for error in e:
                    form.add_error("password", str(error))
                    error = True

            if error:
                error_message = "Error creating new user!"
                return render(request, "add_owner.html",
                              {"form": form, "state": False, "message": error_message})

            photo_b64 = DEFAULT_USER_IMAGE
            if form_data["photo"] is not None:
                photo = form_data["photo"]
                photo_b64 = base64.b64encode(photo.file.read())
                photo_b64 = photo_b64.decode()

            # check for unique constraints
            if User.objects.filter(username=email).exists():
                error_message = "There is already a user with this email! The owner WASN'T added to database!"
                return render(request, "add_owner.html",
                              {"form": form, "state": False, "message": error_message})

            try:
                # create a user
                user = User.objects.create_user(username=email, email=email, first_name=first_name, last_name=last_name,
                                                password=password)

            except Exception:
                error_message = "Error creating new user!"
                return render(request, "add_owner.html",
                              {"form": form, "state": False, "message": error_message})

            try:
                # link the user to a person
                Owner.objects.create(user=user, phone_number=phone_number, photo=photo_b64)

            except Exception:
                user.delete()
                error_message = "Error creating new owner!"
                return render(request, "add_owner.html",
                              {"form": form, "state": False, "message": error_message})

            try:
                if not Group.objects.filter(name="owners_group").exists():
                    Group.objects.create(name="owners_group")

                owners_group = Group.objects.get(name="owners_group")
                owners_group.user_set.add(user)

            except Exception:
                user.delete()
                error_message = "Error creating new owner!"
                return render(request, "add_owner.html",
                              {"form": form, "state": False, "message": error_message})

            return redirect("login")

    else:
        form = AddPerson()

    return render(request, "add_owner.html", {"form": form, "state": None})


@csrf_exempt
def add_client(request):
    if request.method == "POST":
        form = AddPerson(request.POST, request.FILES)
        error = False
        if form.is_valid():

            # get info from data
            form_data = form.cleaned_data
            email = form_data["email"]
            first_name = form_data["first_name"]
            last_name = form_data["last_name"]
            phone_number = form_data["phone_number"]
            password = form_data["password"]
            repeat_password = form_data["repeat_password"]

            try:
                validate_password(password)
                if password != repeat_password:
                    form.add_error("password", "Password must be the same!")
                    form.add_error("repeat_password", "Password must be the same!")
                    error = True
            except ValidationError as e:
                for error in e:
                    form.add_error("password", str(error))
                error = True

            if error:
                error_message = "Error creating new user!"
                return render(request, "add_owner.html",
                              {"form": form, "state": False, "message": error_message})

            photo_b64 = DEFAULT_USER_IMAGE
            if form_data["photo"] is not None:
                photo = form_data["photo"]
                photo_b64 = base64.b64encode(photo.file.read())
                photo_b64 = photo_b64.decode()

            # check for unique constraints
            if User.objects.filter(username=email).exists():
                error_message = "There is already a user with this email! The user WASN'T added to database!"
                return render(request, "add_client.html",
                              {"form": form, "state": False, "message": error_message})

            try:
                # create a user
                user = User.objects.create_user(username=email, email=email, first_name=first_name, last_name=last_name,
                                                password=password)

            except Exception:
                error_message = "Error creating new user!"
                return render(request, "add_client.html",
                              {"form": form, "state": False, "message": error_message})

            try:
                # link the user to a client
                Client.objects.create(user=user, phone_number=phone_number, photo=photo_b64)

            except Exception:
                user.delete()
                error_message = "Error creating new client!"
                return render(request, "add_client.html",
                              {"form": form, "state": False, "message": error_message})

            try:
                if not Group.objects.filter(name="clients_group").exists():
                    Group.objects.create(name="clients_group")

                clients_group = Group.objects.get(name="clients_group")
                clients_group.user_set.add(user)

            except Exception:
                user.delete()
                error_message = "Error creating new client!"
                return render(request, "add_client.html",
                              {"form": form, "state": False, "message": error_message})

            return redirect("login")

    else:
        form = AddPerson()

    return render(request, "add_client.html", {"form": form, "state": None})


@csrf_exempt
def add_restaurant(request):
    # if the user isn't authenticated or is not an owner, the redirect to login
    if not request.user.is_authenticated or (request.user.groups.all()[0].name not in ["owners_group"]):
        return redirect("login")

    if request.method == "POST":
        form = AddRestaurant(request.POST, request.FILES)

        form.is_valid()

        # get info from data
        form_data = form.cleaned_data
        name = form_data["name"]
        address = form_data["address"]
        city = form_data["city"]
        zip_code = form_data["zip_code"]
        country = form_data["country"]
        latitude = form_data["latitude"]
        longitude = form_data["longitude"]
        phone_number = form_data["phone_number"]
        is_open = form_data["is_open"] if "is_open" in form_data else True

        photo_b64 = DEFAULT_RESTAURANT_IMAGE
        if form_data["photo"] is not None:
            photo = form_data["photo"]
            photo_b64 = base64.b64encode(photo.file.read())
            photo_b64 = photo_b64.decode()

        # try to register new restaurant
        if form.is_valid():
            try:
                Restaurant.objects.create(name=name, address=address, city=city, zip_code=zip_code,
                                          country=country, latitude=latitude, longitude=longitude,
                                          is_open=is_open, photo=photo_b64, phone_number=phone_number,
                                          owner=Owner.objects.get(user__username=request.user.username))

            except Exception as e:
                error_message = "Error creating new restaurant!"
                return render(request, "add_restaurant.html",
                              {"form": form, "state": False, "message": error_message})

            message = "Your restaurant was created with success!"

            return render(request, "add_restaurant.html", {"form": form, "state": True, "message": message})

    else:
        form = AddRestaurant()

    return render(request, "add_restaurant.html", {"form": form, "state": None})


@csrf_exempt
def client_profile(request, name=None):
    if not request.user.is_authenticated:
        return redirect("login")

    if name is None:
        name = request.user.username

    form = UpdateClient(None)
    if not Client.objects.filter(user__username=name).exists():
        messages.error(request, "There is  no Client with that email!")
        return render(request, "client_profile.html", {'form': form, "editable": False})

    user = User.objects.get(username=name)
    editable = True

    if request.user.username != name:
        editable = False
        user = User.objects.get(username=name)

    if request.method == "POST":
        if "remove" in request.POST:
            user.delete()
            return redirect("login")

        form = UpdateClient(None, request.POST, request.FILES)

        # get info from Form
        if form.is_valid():
            form_data = form.cleaned_data
            email = form_data["email"]
            first_name = form_data["first_name"]
            last_name = form_data["last_name"]
            phone_number = form_data["phone_number"]
            photo = form_data["photo"]
            new_password = form_data["new_password"]
            repeat_password = form_data["repeat_password"]

            # get user and update it
            u = User.objects.filter(username=email)
            u.update(first_name=first_name, last_name=last_name, email=email)

            if new_password != "":
                if new_password != repeat_password:
                    error_message = "Password must be the same!"
                    messages.error(request, error_message)
                    return render(request, "client_profile.html",
                                  {"form": form})

                usr = User.objects.get(username=email)
                usr.set_password(new_password)
                usr.save()

            # get associated client and update it
            c = Client.objects.filter(user=u[0])
            c.update(phone_number=phone_number)

            # if there is a photo to update
            if photo is not None:
                photo_b64 = base64.b64encode(photo.file.read())
                photo_b64 = photo_b64.decode()
                c.update(photo=photo_b64)

            form = UpdateClient(Client.objects.get(user=user))
        else:
            messages.error(request, "Error")

    else:
        form = UpdateClient(Client.objects.get(user=user))

    return render(request, "client_profile.html", {'form': form, "editable": editable})


@csrf_exempt
def owner_profile(request, name=None):
    if not request.user.is_authenticated:
        return redirect("login")

    if name is None:
        name = request.user.username

    form = UpdateOwner(None)
    if not Owner.objects.filter(user__username=name).exists():
        messages.error(request, "There is no Owner with that email!")
        return render(request, "owner_profile.html", {'form': form, "editable": False})

    user = User.objects.get(username=name)
    editable = True

    if request.user.username != name:
        editable = False
        user = User.objects.get(username=name)

    if request.method == "POST":
        if "remove" in request.POST:
            user.delete()
            return redirect("login")

        form = UpdateOwner(None, request.POST, request.FILES)

        # get info from Form
        if form.is_valid():
            form_data = form.cleaned_data
            email = form_data["email"]
            first_name = form_data["first_name"]
            last_name = form_data["last_name"]
            phone_number = form_data["phone_number"]
            photo = form_data["photo"]
            new_password = form_data["new_password"]
            repeat_password = form_data["repeat_password"]

            # get user and update it
            u = User.objects.filter(username=email)
            u.update(first_name=first_name, last_name=last_name, email=email)

            if new_password != "":
                if new_password != repeat_password:
                    error_message = "Password must be the same!"
                    messages.error(request, error_message)
                    return render(request, "client_profile.html",
                                  {"form": form})

                usr = User.objects.get(username=email)
                usr.set_password(new_password)
                usr.save()

            # get associated client and update it
            o = Owner.objects.filter(user=u[0])
            o.update(phone_number=phone_number)

            # if there is a photo to update
            if photo is not None:
                photo_b64 = base64.b64encode(photo.file.read())
                photo_b64 = photo_b64.decode()
                o.update(photo=photo_b64)

            form = UpdateOwner(Owner.objects.get(user=user))
        else:
            messages.error(request, "Error")

    else:
        form = UpdateOwner(Owner.objects.get(user=user))

    return render(request, "owner_profile.html", {'form': form, "editable": editable})


@csrf_exempt
def owner_restaurants(request):
    if not request.user.is_authenticated or (request.user.groups.all()[0].name not in ["owners_group"]):
        return redirect("login")

    rests = []
    owner = Owner.objects.get(user__username=request.user.username)
    for rest in Restaurant.objects.filter(owner=owner):
        data = {"id": rest.id, "name": rest.name, "address": rest.address, "city": rest.city, "country": rest.country,
                "photo": rest.photo, "is_open": rest.is_open, "phone_number": rest.phone_number}
        reviews = Review.objects.filter(restaurant=rest)
        if not reviews.exists():
            average_score = 0
            n_of_reviews = 0
        else:
            n_of_reviews = len(reviews)
            re_sum = sum([review.stars for review in reviews])
            average_score = round(re_sum / n_of_reviews, 1)
        data["average_score"] = average_score
        data["n_of_reviews"] = n_of_reviews
        rests.append(data)

    return render(request, "owner_restaurants.html", {"rests": rests})


@csrf_exempt
def edit_restaurant(request, number):
    # if the user isn't authenticated or is not an owner, the redirect to login
    if not request.user.is_authenticated or (request.user.groups.all()[0].name not in ["owners_group"]):
        return redirect("login")

    if request.method == "POST":
        if "remove" in request.POST:
            Restaurant.objects.get(id=number).delete()
            return redirect("my-restaurants")

        form = EditRestaurant(None, request.POST, request.FILES)
        if form.is_valid():

            # get info from data
            form_data = form.cleaned_data
            address = form_data["address"]
            zip_code = form_data["zip_code"]
            latitude = form_data["latitude"]
            longitude = form_data["longitude"]
            photo = form_data["photo"]
            phone_number = form_data["phone_number"]

            # get restaurant and update it
            r = Restaurant.objects.filter(id=number)
            r.update(address=address, zip_code=zip_code, latitude=latitude, longitude=longitude,
                     phone_number=phone_number)

            # if there is a photo to update
            if photo is not None:
                photo_b64 = base64.b64encode(photo.file.read())
                photo_b64 = photo_b64.decode()
                r.update(photo=photo_b64)

            form = EditRestaurant(Restaurant.objects.get(id=number))

        else:
            messages.error(request, "Error")
            return render(request, "edit_restaurant.html", {'form': form})

    else:
        form = EditRestaurant(Restaurant.objects.get(id=number))

    form = EditRestaurant(Restaurant.objects.get(id=number))
    return render(request, "edit_restaurant.html", {'form': form})


@csrf_exempt
def restaurant_detail(request, number):
    if not Restaurant.objects.filter(id=number).exists():
        return redirect("home")

    rest = Restaurant.objects.get(id=number)
    data = {"id": rest.id, "name": rest.name, "address": rest.address, "city": rest.city, "country": rest.country,
            "latitude": rest.latitude, "longitude": rest.longitude, "photo": rest.photo,
            "is_open": rest.is_open, "phone_number": rest.phone_number, "description": rest.description}

    reviews = Review.objects.filter(restaurant=rest)
    if not reviews.exists():
        average_score = 0
        n_of_reviews = 0
    else:
        n_of_reviews = len(reviews)
        re_sum = sum([review.stars for review in reviews])
        average_score = round(re_sum / n_of_reviews, 1)
    data["average_score"] = average_score
    data["n_of_reviews"] = n_of_reviews

    reviews_data = []
    for review in reviews:
        review_data = {"title": review.title, "comment": review.comment, "score": review.stars,
                       "timestamp": review.timestamp}
        client = Client.objects.get(user_id=review.user_id)
        review_data["user"] = {"first_name": client.user.first_name, "last_name": client.user.last_name,
                               "photo": client.photo, "n_of_reviews": len(Review.objects.filter(user=client)),
                               "email": client.user.email}
        reviews_data.append(review_data)

    data["reviews"] = reviews_data
    o = Owner.objects.get(user_id=rest.owner_id)
    data["owner"] = {"first_name": o.user.first_name, "last_name": o.user.last_name, "photo": o.photo,
                     "n_of_rests": len(Restaurant.objects.filter(owner=o)), "email": o.user.email}

    is_authenticated = request.user.is_authenticated
    is_owner = False
    is_favorite = False
    if is_authenticated:
        if Owner.objects.filter(user=request.user).exists():
            owner = Owner.objects.get(user=request.user)
            if Restaurant.objects.filter(id=number, owner=owner).exists():
                is_owner = True
        else:
            is_favorite = rest in Client.objects.get(user=request.user).favorites.iterator()

    if request.method == "POST":
        if "fav" in request.POST:
            if is_authenticated and request.user.groups.all()[0].name in ["clients_group"]:
                client = Client.objects.get(user=request.user)
                if rest not in client.favorites.iterator():
                    client.favorites.add(rest)
                else:
                    client.favorites.remove(rest)
            return redirect(f"/restaurant/{number}")

        form = AddReview(request.POST, request.FILES)

        if form.is_valid():
            form_data = form.cleaned_data

            comment = form_data["comment"]
            title = form_data["title"]
            score = form_data["score"]
            price = form_data["price"]
            clean = form_data["cleanliness"]
            quality = form_data["quality"]
            speed = form_data["speed"]

            client = Client.objects.get(user=request.user)
            if Review.objects.filter(restaurant=rest, user=client).exists():
                r = Review.objects.filter(restaurant=rest, user=client)
                r.update(comment=comment, stars=score, price=price, title=title, cleanliness=clean, quality=quality,
                         speed=speed)

            else:
                Review.objects.create(restaurant=rest, user=Client.objects.get(user=request.user), comment=comment,
                                      stars=score, price=price, title=title, cleanliness=clean, quality=quality,
                                      speed=speed)
            return redirect(f"/restaurant/{number}")
        else:
            return redirect(f"/restaurant/{number}")

    else:
        form = AddReview()

    return render(request, "restaurant_detail.html",
                  {"is_favorite": is_favorite, "is_authenticated": is_authenticated, "is_owner": is_owner, "rest": data,
                   "form": form})


def get_restaurant_details(restaurant, data, client=None):
    reviews = Review.objects.filter(restaurant=restaurant)

    if not reviews.exists():
        average_score = 0
        n_of_reviews = 0
        average_price = 0
        average_cleanliness = 0
        average_speed = 0
    else:
        n_of_reviews = len(reviews)

        re_sum = sum([review.stars for review in reviews])
        average_score = round(re_sum / n_of_reviews, 1)

        average_price = round(sum([r.price for r in reviews]) / n_of_reviews, 1)
        average_cleanliness = round(sum([r.cleanliness for r in reviews]) / n_of_reviews, 1)
        average_speed = round(sum([r.speed for r in reviews]) / n_of_reviews, 1)

    data["average_score"] = average_score
    data["n_of_reviews"] = n_of_reviews
    data["average_cleanliness"] = average_cleanliness
    data["average_price"] = average_price
    data["average_speed"] = average_speed

    reviews_data = []
    for review in reviews:
        review_data = {"title": review.title, "comment": review.comment, "score": review.stars,
                       "timestamp": review.timestamp}
        client = Client.objects.get(user_id=review.user_id)
        review_data["user"] = {"first_name": client.user.first_name, "last_name": client.user.last_name,
                               "photo": client.photo, "n_of_reviews": len(Review.objects.filter(user=client))}
        reviews_data.append(review_data)

    data["reviews"] = reviews_data

    o = Owner.objects.get(user_id=restaurant.owner_id)

    data["owner"] = {"first_name": o.user.first_name, "last_name": o.user.last_name, "photo": o.photo,
                     "n_of_rests": len(Restaurant.objects.filter(owner=o))}


@csrf_exempt
def restaurant_listing(request):
    if '_post_data' in request.session and request.session['_post_data'] is not None:
        post_data = request.session.get('_post_data')
        del request.session['_post_data']
    else:
        post_data = {}
    fav_rest_id = None

    if request.method == "POST":
        elems = [s for s in request.POST if 'fav' in s]
        if elems:
            fav_rest_id = int(elems[0].strip('fav'))

            form = SearchRestaurant()
        else:
            form = SearchRestaurant(request.POST, request.FILES)

            if form.is_valid():
                form_data = form.cleaned_data

                sort_by = form_data["sort_by"]
                name = form_data["name"]
                city = form_data["city"]

                if sort_by:
                    post_data['sort_by'] = sort_by
                if name:
                    post_data['name'] = name
                if city:
                    post_data['city'] = city

            else:
                return redirect('restaurant-listing')
    else:
        form = SearchRestaurant()

    if 'name' not in post_data and 'city' not in post_data:
        query_data = Restaurant.objects.all()
    elif 'name' not in post_data:
        city = post_data['city']
        query_data = Restaurant.objects.filter(city__icontains=city)
    elif 'city' not in post_data:
        name = post_data['name']
        query_data = Restaurant.objects.filter(name__icontains=name)
    else:
        name = post_data['name']
        city = post_data['city']
        query_data = Restaurant.objects.filter(name__icontains=name, city__icontains=city)

    is_authenticated = request.user.is_authenticated
    is_owner = False

    client = None
    if is_authenticated and request.user.groups.all()[0].name in ["clients_group"]:
        client = Client.objects.get(user__username=request.user.username)

    if fav_rest_id is not None:
        rest = Restaurant.objects.get(id=fav_rest_id)
        if is_authenticated and client is not None:
            if not rest in client.favorites.iterator():
                client.favorites.add(rest)
            else:
                client.favorites.remove(rest)

    data = []
    for rest in query_data:
        elem_data = {"id": rest.id, "name": rest.name, "address": rest.address, "city": rest.city,
                     "country": rest.country,
                     "latitude": rest.latitude, "longitude": rest.longitude, "photo": rest.photo,
                     "is_open": rest.is_open, "phone_number": rest.phone_number, "description": rest.description}

        if is_authenticated and client is not None:
            get_restaurant_details(rest, elem_data, client)
            elem_data["user_fav"] = rest in client.favorites.all()
        else:
            get_restaurant_details(rest, elem_data)

        data.append(elem_data)

    if 'sort_by' in post_data:
        sort_method = int(post_data['sort_by'])
        if sort_method == 1:
            data.sort(key=lambda e: e['average_price'])
        elif sort_method == 2:
            data.sort(key=lambda e: e['average_price'], reverse=True)
        elif sort_method == 3:
            data.sort(key=lambda e: e['name'])
        elif sort_method == 4:
            data.sort(key=lambda e: e['name'], reverse=True)
        elif sort_method == 5:
            data.sort(key=lambda e: e['average_cleanliness'], reverse=True)  # descending by default (best to worst)
        elif sort_method == 6:
            data.sort(key=lambda e: e['average_score'])
        elif sort_method == 7:
            data.sort(key=lambda e: e['average_score'], reverse=True)
        elif sort_method == 8:
            data.sort(key=lambda e: e['n_of_reviews'], reverse=True)
        elif sort_method == 9:
            data.sort(key=lambda e: e['average_speed'], reverse=True)  # descending by default (best to worst)
    else:  # default order
        data.sort(key=lambda e: e['average_score'] + e['average_speed'] + e['average_cleanliness'], reverse=True)

    # request.session['_post_data'] = post_data

    return render(request, "restaurant_listing.html",
                  {"is_authenticated": is_authenticated, "is_owner": is_owner, "restaurants": data, "form": form})


@csrf_exempt
def favorites_listing(request):
    if not request.user.is_authenticated or (request.user.groups.all()[0].name not in ["clients_group"]):
        return redirect("login")

    user = request.user
    fav_rest_id = None

    if request.method == "POST":
        elems = [s for s in request.POST if 'fav' in s]
        if elems:
            fav_rest_id = int(elems[0].strip('fav'))

    is_authenticated = request.user.is_authenticated

    client = None
    if is_authenticated and request.user.groups.all()[0].name in ["clients_group"]:
        client = Client.objects.get(user__username=user.username)

    if fav_rest_id is not None:
        rest = Restaurant.objects.get(id=fav_rest_id)
        if is_authenticated and client is not None:
            if rest not in client.favorites.iterator():
                client.favorites.add(rest)
            else:
                client.favorites.remove(rest)

    rests = []

    for rest in client.favorites.all():
        elem_data = {"id": rest.id, "name": rest.name, "address": rest.address, "city": rest.city,
                     "country": rest.country,
                     "photo": rest.photo, "is_open": rest.is_open, "phone_number": rest.phone_number}
        reviews = Review.objects.filter(restaurant=rest)

        if is_authenticated and client is not None:
            get_restaurant_details(rest, elem_data, client)
        else:
            get_restaurant_details(rest, elem_data)

        elem_data['user_fav'] = True

        rests.append(elem_data)

    return render(request, "favorites_listing.html", {"rests": rests})


def reload_database(request):
    #######################################
    #          WIPE DATABASE              #
    #######################################
    for owner in Owner.objects.all():
        owner.user.delete()

    for client in Client.objects.all():
        client.user.delete()

    #######################################
    #          CREATE CLIENTS             #
    #######################################
    u1 = User.objects.create_user(username="client1@ua.pt", email="client1@ua.pt", first_name="James",
                                  last_name="Thompson", password="client1")

    u2 = User.objects.create_user(username="client2@ua.pt", email="client2@ua.pt", first_name="Patricia",
                                  last_name="Clark", password="client2")

    u3 = User.objects.create_user(username="client3@ua.pt", email="client3@ua.pt", first_name="Paul",
                                  last_name="Turner", password="client3")

    c1 = Client.objects.create(user=u1, phone_number="913426890", photo=CLIENT_1)

    c2 = Client.objects.create(user=u2, phone_number="925385124", photo=CLIENT_2)

    c3 = Client.objects.create(user=u3, phone_number="963057208", photo=CLIENT_3)

    if not Group.objects.filter(name="clients_group").exists():
        Group.objects.create(name="clients_group")

    clients_group = Group.objects.get(name="clients_group")
    clients_group.user_set.add(u1)
    clients_group.user_set.add(u2)
    clients_group.user_set.add(u2)

    #######################################
    #          CREATE OWNERS              #
    #######################################
    u4 = User.objects.create_user(username="owner1@ua.pt", email="owner1@ua.pt", first_name="John",
                                  last_name="Smith", password="owner1")

    u5 = User.objects.create_user(username="owner2@ua.pt", email="owner2@ua.pt", first_name="Mary",
                                  last_name="Lewis", password="owner2")

    u6 = User.objects.create_user(username="owner3@ua.pt", email="owner3@ua.pt", first_name="Helen",
                                  last_name="Martinez", password="owner3")

    o1 = Owner.objects.create(user=u4, phone_number="928470213", photo=OWNER_1)

    o2 = Owner.objects.create(user=u5, phone_number="910023459", photo=OWNER_2)

    o3 = Owner.objects.create(user=u6, phone_number="931734580", photo=OWNER_3)

    if not Group.objects.filter(name="owners_group").exists():
        Group.objects.create(name="owners_group")

    owners_group = Group.objects.get(name="owners_group")
    owners_group.user_set.add(u4)
    owners_group.user_set.add(u5)
    owners_group.user_set.add(u6)

    #######################################
    #          CREATE RESTAURANTS         #
    #######################################
    r1 = Restaurant.objects.create(name="Emerald Chinese Restaurant", address="30 Eglinton Avenue W",
                                   description="Emerald Chinese Cuisine is one of San Diego's premiere dim sum and Cantonese style restaurant. It presents traditional Chiese flavors in an elegant, contemporary setting in the Convoy Street district of San Diego.  A lavish and jewel-toned dining space draws inspiration from elements of traditional Chinese architecture and contemporary designs that are stunning and eclectic. The elegant surroundings pair perfectly with emerald menu - featuring delicacies like all day al a cart dim sum, Cantonese style Chilean seabass and one of the most popular emerald dishes, table-side carved Peking duck.",
                                   city="Mississauga", zip_code="L5R 3E7", country="Canada", latitude=43.6054989743,
                                   longitude=-79.652288909, is_open=True, phone_number=2142436323, photo=REST_1,
                                   owner=o1)

    r2 = Restaurant.objects.create(name="Musashi Japanese Restaurant", address="10110 Johnston Rd, Ste 15",
                                   description="Established in 1996.  Musashi Japanese Restaurant and Sushi Bar has a long history of providing the Palm Desert and the Valley with the highest quality Japanese cuisine prepared skillfully.",
                                   city="Charlotte", zip_code="28210", country="USA", latitude=35.092564,
                                   longitude=-80.859132, is_open=False, phone_number=4324798765, photo=REST_2, owner=o2)

    r3 = Restaurant.objects.create(name="Marco's Pizza", address="5981 Andrews Rd",
                                   description="Marco's Pizza, operated by Marco's Franchising, LLC, is an American restaurant chain and interstate franchise based in Toledo, Ohio, that specializes in Italian-American cuisine.",
                                   city="Mentor-on-the-Lake", zip_code="44060", country="USA", latitude=41.70852,
                                   longitude=-81.359556, is_open=True, phone_number=4324798765, photo=REST_3, owner=o3)

    r4 = Restaurant.objects.create(name="Carluccio's Tivoli Gardens", address="1775 E Tropicana Ave",
                                   description="Carluccio's Tivoli Gardens is an Italian restaurant in Las Vegas Nevada. The restaurant was designed by non-other than Liberace who owned the restaurant as Liberace's Tivoli Gardens.",
                                   city="Las Vegas", zip_code="44060", country="USA", latitude=36.1000163,
                                   longitude=-115.1285285, is_open=False, phone_number="(702) 795-3236", photo=REST_4,
                                   owner=o3)

    r5 = Restaurant.objects.create(name="Ipanema Restaurant", address="43 W 46th St",
                                   description="Ipanema is a Brazilian/Portuguese restaurant on Little Brazil street in Midtown West. We feature authentic, homestyle dishes and a variety of cocktails, including our infamous Caipirinha. Guests are welcome for a quick bite or speciality cocktail at the bar, or for a long pleasant dining experience in our relaxed yet unique atmosphere. Some more things we specialize in: Steak, Fish, Seafood, Caipirinhas, Crochets, Paelha, Lobster, Shrimp, Fried Bananas, Black bean soup Mais coisas que se especializam em: Feijoada, Churrasco Gaucho, Costeleta de Porco a Portuguesa, Farofa, Couve, Sopa de Feijão, Banana Frita , Bolhão de Pato, Camarão no Côco, Ipanema Restaurant Serving Little Brazil Street Since 1979",
                                   city="New York", zip_code="NY 10036", country="USA", latitude=40.757004,
                                   longitude=-73.980624, is_open=True, phone_number="+1-646-791-7171", photo=REST_5,
                                   owner=o2)

    r6 = Restaurant.objects.create(name="Burger & Lobster", address="39 W 19th St",
                                   description="We focus our energy on making two products the absolute best they can be without any other distractions. We’re obsessed with refining our techniques to specialise in the simplicity of our mono-product offering in the most fascinating locations in the world. We craft prime burgers and serve wild, fresh Atlantic lobsters.",
                                   city="New York", zip_code="NY 10011", country="USA", latitude=40.740367,
                                   longitude=-73.993430, is_open=True, phone_number="+1-646-791-7171", photo=REST_6,
                                   owner=o1)

    r7 = Restaurant.objects.create(name="Paesano of Mulberry Street", address="136 Mulberry St",
                                   description="Nestled in the heart of Little Italy, Paesano's epitomizes Italian culture and charm. Try our Osso Buco veal - shank cooked in its own juices served over rice. In the mood for seafood? Try our Seafood Cartoccio lobster, shrimp, clams, mussels, red snapper, calamari, steamed in white wine served with a side of linguini and clam sauce. Today, the recipes brought from old-world Italy are still cherished and prepared with love at Paesano's of Mulberry Street. For authentic Italian cooking and warm hospitality that is customary in Italian tradition, Paesano's is your Little Italy dinning destination.",
                                   city="New York", zip_code="NY 10013", country="USA", latitude=40.718783,
                                   longitude=-73.997164, is_open=True, phone_number="+1-212-965-1188", photo=REST_7,
                                   owner=o3)

    #######################################
    #            CREATE REVIEWS           #
    #######################################
    Review.objects.create(restaurant=r7, user=c1, title="Precious food!",
                          comment="So this gem, I spontaneously tried while cruising through Little Italy.\nLuckily I was not disappointed. The hospitality and service was great from start to finish.\nMy sister arrived early and they gave her a glass of red wine on the house.\nEveryone at the table was extremely satisfied with the entres. I had the Carbonara, my sister the Eggplant Parmesan, and our third guest the Spaghetti Bolognese.\nSpeaking from the perspective of an individual who went on a pasta marathon in Italy, the pasta at Paesano's tasted identical to what I had across different Italian cities.\nThe pasta was perfectly cooked and fused perfectly with the sauce, meat etc.\nI highly recommend Paesano's. You taste the passion and authentic flavors in their food.",
                          stars=7.8, price=2, cleanliness=3, quality=2, speed=1)

    Review.objects.create(restaurant=r7, user=c3, title="Kind of disappointed!",
                          comment="We were excited to try this restaurant, we just found it on yelp randomly (we were visiting from out of town). We were sat promptly, and given bread and drinks right Away. We did like the interior, it was inviting and comfy. We loved the bread and oil, that was the tastiest thing we ate for sure.\nWe ordered the Rigitoni and fettuccini in the vodka sauce. We were so excited to try it but unfortunately is was pretty bland. We were expecting much more flavor.\nFor the price I think we could have gotten something much better elsewhere. Didn't try anything else, but don't think we would return.",
                          stars=5.3, price=3, cleanliness=2, quality=2, speed=2)

    Review.objects.create(restaurant=r6, user=c2, title="Great time, great food!",
                          comment="Been there for the first time and I liked it, the food is good as a burger love I can tell when one is good and one isnt and I can say this was amazing... The lobsters were normal they kinda taste all same to me\nThe service is good and people is pretty nice, a really lovely place.\nI'll be coming more often with no doubt.",
                          stars=9.8, price=2, cleanliness=3, quality=3, speed=3)

    Review.objects.create(restaurant=r6, user=c1, title="Amazing experience",
                          comment="Been here three times and loved it each time. It's also by my office so it's super convenient to swing by.\nRestaurant is huge with tons of seating which is great if you ever want to host a dinner with friends here.\nI ordered calamari and a combo - which offers both burger and lobster (highly recommend). Only $35 to get both and very deliciously juicy. It's also very filling so had to bring it back home and ate it for my next meal haha\n10/10 would def recommend",
                          stars=10, price=2, cleanliness=3, quality=3, speed=3)

    Review.objects.create(restaurant=r6, user=c3, title="Good food...",
                          comment="I ordered Lobster bisque and Combo for 1.  It was too much food for one person.\nBisque is Ok, but I have had better.\nLobster is very good.\nBurger is OK. Burgers all taste the same for me. Probably shouldnt have ordered the combo.  Lobster alone would have been sufficient.\nService is very good.",
                          stars=7.5, price=2, cleanliness=3, quality=3, speed=3)

    Review.objects.create(restaurant=r5, user=c3, title="Kind of good!",
                          comment="If you are looking for a great Brazilian restaurant, head over to Ipanema. You will not be disappointed in any way. The food is incredible! We started with mixed salgadinhos (empanadas), which were delicious. We ordered The Picanha steak and the Vatapá ( shrimp in coconut  milk and palm oil), which was my absolute favorite.\nWhile the food was incredible, the service was beyond belief. My husband is from Rio, but our guests had never tried Brazilian food. Our waiter, Cadu, was amazing! So attentive, patient and informative, he really made our night.\nThanks to the entire staff for an incredible evening. We felt like family.",
                          stars=6, price=2, cleanliness=2, quality=2, speed=2)

    Review.objects.create(restaurant=r4, user=c2, title="Nothing special",
                          comment="We asked our hotel concierge for a restaurant recommendation.  She said there's a place off the strip where you can get excellent cheap Italian food.  She said, 'I'm Italian' and I love it and all my out of town guests always ask to go there.  So we said what the heck, beats paying a fortune for a mediocre meal at one of the big fancy hotel restaurants.  \nThe first note about Carluccio's is it is right next door to the Liberace Museum.  Las Vegas is such a bizarre place.  Any way, the interior is a flash back to to the 80's and still kind of charming in that bizarre Las Vegas sort of way.  It was also dark inside, which was a good thing, because they would get negative stars for the cleanliness factor.  Our bread basket and the inside of my menu had globs of marinara sauce on them.  Tasty....um, no....disgusting.\nThe bread was outstanding.  It was just bread, with no fancy dipping oil or vinegar. Just good solid bread with butter.  Side salad...blech.  Bottle of Pinot Grigio...yummy (friend pointed out it could be bought at Costco for half the price we paid).  Mushroom appetizer...good in concept...mushrooms in white wine sauce topped with cheese and baked.  Not so good in execution as the wine sauce was tangy and harsh.  Chicken parmigiana entree, pretty tasty.  It was served with a side of pasta with meat sauce containing very little meat to speak of.  I tried my friend;s gnocchi and it was doughy tasting (not in a good way)!  We skipped dessert.\nService was poor, but what do you expect when  you can get a bottle of wine, 3 entrees, 3 side salads and an appetizer for less than $80 (the wine was half the bill at $40).  \nThis place would be great for a group of people who want decent food for cheap.  Plus Liberace is next door.  That's a big draw right there!  Or not...",
                          stars=4, price=3, cleanliness=2, quality=2, speed=1)

    Review.objects.create(restaurant=r3, user=c1, title="Just food...",
                          comment="I have been wanting to try Marco's Pizza for a while.\nThey have an online deal for medium one-topping pizzas for $5.99/ea so I ordered one in pepperoni and one in pineapple both original crust and both with garlic sauce on the crust.  I also tried chicken wings and cinnasqaures.\nOut of the different food I tried there the wings were my favorite.  Great taste and great wing portion.  The pizzas were decent, but really nothing tasted different from pizza chains.  I was a little disappointed at that.  Even with the garlic sauce on the crust, I couldn't taste it at all.  Also, original crust is too bready.  Next time I'll opt for lite or thin crust and a different crust sauce as well.  I highly do not suggest getting the cinnasquares.  The bread part of it was okay, but the artificial icing is a no-go.  \nService was great and the restaurant itself wasn't bad.  They do serve beer and have a TV which is great for games.  Overall, I'd come back here again for pizza.",
                          stars=6.5, price=2, cleanliness=2, quality=2, speed=1)

    Review.objects.create(restaurant=r2, user=c2, title="Great time, great food!",
                          comment="If you're looking for authentic Japanese food, look no further.\nIt's a small space, so expect to wait on weekends. They do not rush you, and things are prepared fresh so it will take some time to dine.\nThe bento box is my go to, as it has a little bit of everything and you can't beat the price! Delicious food, kind people, and a relaxed atmosphere.",
                          stars=9, price=2, cleanliness=2, quality=2, speed=3)

    Review.objects.create(restaurant=r1, user=c3, title="Pretty good..",
                          comment="Pretty good place to go with family for dim sum.\nThis is one of those restaurants where they walk around with carts of food and you point at what you want, they give it to you and stamp your card.\nWe came here just for some lunch with four people on Victoria day. Got seated quickly, but the restaurant was pretty packed.\nSome of the woman serving food didn't know much English and if you pointed by accident while talking to your table, they thought you wanted it and proceeded to try to place it on your table. Weird...\nThe most disappointing thing was that we wanted the most popular dim sum (shumai) but they didn't have any in the two hours we were there! They had other things except for shumai and when you asked for it, they tried to give you a mushroom dim sum instead...",
                          stars=6, price=2, cleanliness=2, quality=2, speed=2)

    #######################################
    #          CREATE FAVORITES           #
    #######################################
    c1.favorites.add(r7)
    c1.favorites.add(r4)
    c1.favorites.add(r6)
    c2.favorites.add(r2)
    c2.favorites.add(r4)
    c3.favorites.add(r1)

    return redirect("home")
